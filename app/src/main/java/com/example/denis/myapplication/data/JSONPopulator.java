package com.example.denis.myapplication.data;

import org.json.JSONObject;

/**
 * Created by Denis on 19.04.2016.
 */
public interface JSONPopulator {
    void populate(JSONObject data);
}
