package com.example.denis.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.denis.myapplication.data.Channel;
import com.example.denis.myapplication.data.Item;
import com.example.denis.myapplication.service.WeatherServiceCallback;
import com.example.denis.myapplication.service.YahooWeatherService;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Denis on 19.04.2016.
 */
public class WeatherActivity extends AppCompatActivity implements WeatherServiceCallback {

    private ImageView weatherIconImageView;
    private TextView temperatureTextView;
    private TextView conditionTextView;
    private TextView locationTextView;
    public static String Location = "null";
    private YahooWeatherService service;
    private ProgressDialog dialog;
    public static double latitude;
    public static double longitude;
    //private float fLatt = (float)latitude;
    //public static double latitude = 46.624035;
    //public static double longitude = 32.734922;
    private String city = "null";
    private String country = "null";
    public static final String APP_PREFERENCES = "settings";
    public static final String APP_PREFERENCES_LOCATION = "Location";
    //public static final String APP_PREFERENCES_LATITUDE = "latitude";
    //public static final String APP_PREFERENCES_LONGITUDE = "longitude";
    private SharedPreferences Settings;
    private static boolean s = false;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        weatherIconImageView = (ImageView)findViewById(R.id.WeatherIconImageView);
        temperatureTextView = (TextView)findViewById(R.id.temperatureTextView);
        conditionTextView = (TextView)findViewById(R.id.conditionTextView);
        locationTextView = (TextView)findViewById(R.id.locationTextView);
        Settings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if (s == false){
            Location = Settings.getString(APP_PREFERENCES_LOCATION,"null");
            //latitude = Settings.getFloat(APP_PREFERENCES_LATITUDE,(float) latitude);
            //longitude = Settings.getFloat(APP_PREFERENCES_LONGITUDE,(float)longitude);
            s = true;
        }else {getLocationAddress();}
        service = new YahooWeatherService(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading....");
        dialog.show();

        service.refreshWeather(Location);
        }




    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();

        Item item = channel.getItem();
        int resourceId = getResources().getIdentifier("drawable/icon_" + item.getCondition().getCode(), null, getPackageName());

        @SuppressWarnings("deprecation")
        Drawable weatherIconDrawble = getResources().getDrawable(resourceId);


        weatherIconImageView.setImageDrawable(weatherIconDrawble);

        temperatureTextView.setText(item.getCondition().getTemperature() + "\u00B0" + channel.getUnits().getTemperature());
        conditionTextView.setText(item.getCondition().getDescription());
        locationTextView.setText(service.getLocation());
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    public void onMapMenuClick(MenuItem item){
        Intent intent = new Intent(WeatherActivity.this,MapsActivity.class);
        startActivity(intent);
    }

    public void refreshWeather(MenuItem item){
        service = new YahooWeatherService(this);
        dialog = new ProgressDialog(this);
        service.refreshWeather(Location);
        dialog.setMessage("Refreshing....");
        dialog.show();
    }

    public void setDefault (MenuItem item){
        SharedPreferences.Editor editor = Settings.edit();
        editor.putString(APP_PREFERENCES_LOCATION, Location);
        //editor.putFloat(APP_PREFERENCES_LATITUDE,(float) latitude);
        //editor.putFloat(APP_PREFERENCES_LONGITUDE,(float) longitude);
        editor.apply();
    }

    public void getLocationAddress(){
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()){
            city = addresses.get(0).getLocality();
                country = addresses.get(0).getCountryName();
                if (city != null){
                int i = city.indexOf("'");
                if (i != -1) {
                    city = city.replace("'", "");
                }}
                Location = city + ", " +country;


        }
            else Toast.makeText(getApplicationContext(),"Chose city",Toast.LENGTH_LONG).show();
        }

        catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Could not generate address",Toast.LENGTH_LONG).show();
        }
    }



}
