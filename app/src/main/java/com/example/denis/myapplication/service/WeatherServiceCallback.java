package com.example.denis.myapplication.service;

import com.example.denis.myapplication.data.Channel;

/**
 * Created by Denis on 19.04.2016.
 */
public interface WeatherServiceCallback {
    void serviceSuccess (Channel channel);

    void serviceFailure (Exception exception);
}
