package com.example.denis.myapplication.data;

import org.json.JSONObject;

/**
 * Created by Denis on 19.04.2016.
 */
public class Units implements JSONPopulator {
    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void populate(JSONObject data) {

        temperature = data.optString("temperature");

    }
}
